#include<stdio.h>
#define RANGE 10
int main ()
{
    int input=0, i , j;
    printf  ("Enter a number (Multiplication will be printed from 1 to entered number) : - ");
    scanf ("%d", &input);
    for ( int i=1; i<=input; i++)
    {
        for ( int j=1; j <= RANGE; j++)
        {
            printf ("%2d  * %2d = %2d\n",i,j, i * j);
        }
        printf ("\n");
    }
    getch();
    return 0;
}

